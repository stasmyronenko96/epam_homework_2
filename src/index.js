const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');

const { apiRouter } = require('./api/apiRouter');

require('dotenv').config();

const app = express();

mongoose.connect('mongodb+srv://admin:admin@epamhw3.zn9dcus.mongodb.net/?retryWrites=true&w=majority');

const corsOptions = {
  origin: '*',
  credentials: true,
  optionSuccessStatus: 200,
};

app.use(cors(corsOptions));

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api', apiRouter);

const { User } = require('./models/User');

const getAllUsers = (req, res, next) => {
  User.find().then((book) => {
    res.json(book);
    next();
  });
};

const { Note } = require('./models/Note');

const getAllNotes = (req, res, next) => {
  Note.find().then((book) => {
    res.json(book);
    next();
  });
};

app.get('/', getAllUsers);
app.delete('/', async (req, res, next) => {
  await User.findByIdAndDelete('62fd3f1abb342d8973fcecae');
  await res.json({ message: 'Ok' });
  next();
});

app.get('/notes', getAllNotes);
app.delete('/notes', async (req, res, next) => {
  await Note.findByIdAndDelete('62f81f00a65a0fed4b554510');
  await res.json({ message: 'Ok' });
  next();
});

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

function errorHandler(err, req, res, next) {
  res.status(500).send({ message: `Server error: ${err}` });
  next();
}

app.use(errorHandler);
