const mongoose = require('mongoose');

const Note = mongoose.model('Note', {
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
  },
});

module.exports = {
  Note,
};
