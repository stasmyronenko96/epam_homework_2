const express = require('express');

const { registerUser, loginUser } = require('./authService');

const router = express.Router();

router.post('/login', loginUser);

router.post('/register', registerUser);

module.exports = {
  authRouter: router,
};
