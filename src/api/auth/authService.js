const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../../models/User');

async function registerUser(req, res, next) {
  const { username, password } = req.body;
  try {
    const user = new User({
      username,
      password: await bcrypt.hash(password, 10),
      createdDate: (new Date()).toLocaleString(),
    });
    await user.save();
    await res.status(200).send({ message: 'Success' });
  } catch (err) {
    await res.status(400).send({ message: `Error: ${err.message}` });
    return;
  }
  await next();
}

async function loginUser(req, res, next) {
  const user = await User.findOne({ username: req.body.username });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { user: user.username };
    const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
    await res.status(200).json({
      message: 'Success',
      jwt_token: jwtToken,
    });
    await next();
    return;
  }
  await res.status(400).json({ message: 'Invalid password or login' });
}

module.exports = {
  registerUser,
  loginUser,
};
