const express = require('express');
const {
  getUsersNotes,
  addUsersNote,
  getUsersNoteById,
  updateUsersNoteById,
  changeCheckUsersNoteById,
  deleteUsersNoteById,
} = require('./notesServise');

const router = express.Router();

router.get('/', getUsersNotes);

router.post('/', addUsersNote);

router.get('/:id', getUsersNoteById);

router.put('/:id', updateUsersNoteById);

router.patch('/:id', changeCheckUsersNoteById);

router.delete('/:id', deleteUsersNoteById);

module.exports = {
  notesRouter: router,
};
