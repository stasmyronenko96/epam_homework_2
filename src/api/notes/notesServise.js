const { Note } = require('../../models/Note');
const { User } = require('../../models/User');

const getUsersNotes = async (req, res, next) => {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const user = await User.findOne({ username: req.user.user });
    const offset = req.query.offset || 0;
    const limit = req.query.limit || 0;
    // eslint-disable-next-line no-underscore-dangle
    const allNotes = await Note.find({ userId: user._id });
    const resNotes = allNotes.slice(offset, limit ? offset + limit : undefined);
    await res.status(200).json({
      offset,
      limit,
      count: resNotes.length,
      notes: resNotes,
    });
    await next();
  } catch (err) {
    await res.status(400).json({ message: `Error: ${err.message}` });
  }
};

const addUsersNote = async (req, res, next) => {
  try {
    const { text } = req.body;
    const date = (new Date()).toLocaleString();
    const user = await User.findOne({ username: req.user.user });
    const note = new Note({
      // eslint-disable-next-line no-underscore-dangle
      userId: user._id,
      text,
      createdDate: date,
    });
    await note.save();
    await res.status(200).send({ message: 'Success' });
    await next();
  } catch (err) {
    await res.status(400).send({ message: `Error: ${err.message}` });
  }
};

const getUsersNoteById = async (req, res, next) => {
  try {
    const user = await User.findOne({ username: req.user.user });
    const notesId = req.params.id;
    // eslint-disable-next-line no-underscore-dangle
    const note = await Note.findOne({ userId: user._id, _id: notesId });
    if (note) {
      await res.status(200).send({ note });
      await next();
    } else {
      await res.status(400).send({ message: 'Error: Can\'t find this note' });
    }
  } catch (err) {
    await res.status(400).send({ message: `Error: ${err.message}` });
  }
};

const updateUsersNoteById = async (req, res, next) => {
  try {
    const user = await User.findOne({ username: req.user.user });
    const notesId = req.params.id;
    // eslint-disable-next-line no-underscore-dangle
    const note = await Note.findOne({ userId: user._id, _id: notesId });
    if (note) {
      note.text = req.body.text;
      await note.save();
      await res.status(200).send({ message: 'Success' });
      await next();
    } else {
      await res.status(400).send({ message: 'Error: Can\'t find this note' });
    }
  } catch (err) {
    await res.status(400).send({ message: `Error: ${err.message}` });
  }
};

const changeCheckUsersNoteById = async (req, res, next) => {
  try {
    const user = await User.findOne({ username: req.user.user });
    const notesId = req.params.id;
    // eslint-disable-next-line no-underscore-dangle
    const note = await Note.findOne({ userId: user._id, _id: notesId });
    if (note) {
      note.completed = !note.completed;
      await note.save();
      await res.status(200).send({ message: 'Success' });
      await next();
    } else {
      await res.status(400).send({ message: 'Error: Can\'t find this note' });
    }
  } catch (err) {
    await res.status(400).send({ message: `Error: ${err.message}` });
  }
};

const deleteUsersNoteById = async (req, res, next) => {
  try {
    const user = await User.findOne({ username: req.user.user });
    const notesId = req.params.id;
    // eslint-disable-next-line no-underscore-dangle
    await Note.findOneAndDelete({ userId: user._id, _id: notesId });

    await res.status(200).send({ message: 'Success' });
    await next();
  } catch (err) {
    await res.status(400).send({ message: `Error: ${err.message}` });
  }
};

module.exports = {
  getUsersNotes,
  addUsersNote,
  getUsersNoteById,
  updateUsersNoteById,
  changeCheckUsersNoteById,
  deleteUsersNoteById,
};
