const express = require('express');

const { authMiddleware } = require('../middlewares/authMiddleware');

const { authRouter } = require('./auth/authRouter');
const { userRouter } = require('./users/userRouter');
const { notesRouter } = require('./notes/notesRouter');

const router = express.Router();

router.use('/auth', authRouter);
router.use('/users/me', authMiddleware, userRouter);
router.use('/notes', authMiddleware, notesRouter);

module.exports = {
  apiRouter: router,
};
