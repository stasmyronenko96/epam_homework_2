const bcrypt = require('bcryptjs');
const { User } = require('../../models/User');

const getUsersProfile = async (req, res, next) => {
  if (req.user.user) {
    const user = await User.findOne({ username: req.user.user });
    await res.status(200).json({
      user: {
        // eslint-disable-next-line no-underscore-dangle
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
      },
    });
    await next();
  } else {
    await res.status(400).json({ message: 'User didn\'t find' });
  }
};

const deleteUserProfile = async (req, res, next) => {
  if (req.user.user) {
    await User.findOneAndDelete({ username: req.user.user });
    await res.status(200).json({ message: 'Success' });
    await next();
  } else {
    await res.status(400).json({ message: 'User didn\'t find' });
  }
};

const changeUsersPassword = async (req, res, next) => {
  const { oldPassword, newPassword } = req.body;
  if (req.user.user && oldPassword && newPassword) {
    const user = await User.findOne({ username: req.user.user });
    try {
      if (await bcrypt.compare(String(oldPassword), String(user.password))) {
        user.password = await bcrypt.hash(newPassword, 10);
        await user.save();
        await res.status(200).json({ message: 'Success' });
      } else {
        await res.status(400).json({ message: 'Old password invalid' });
      }
      await next();
    } catch (err) {
      await res.status(400).json({ message: `Error: ${err.message}` });
    }
  }
};

module.exports = {
  getUsersProfile,
  deleteUserProfile,
  changeUsersPassword,
};
