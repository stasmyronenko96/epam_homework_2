const express = require('express');

const { getUsersProfile, deleteUserProfile, changeUsersPassword } = require('./userService');

const router = express.Router();

router.get('/', getUsersProfile);

router.delete('/', deleteUserProfile);

router.patch('/', changeUsersPassword);

module.exports = {
  userRouter: router,
};
