const jwt = require('jsonwebtoken');

const authMiddleware = async (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    await res.status(400).json({ message: 'Not authenticate' });
    return;
  }
  const [, token] = authorization.split(' ');
  if (!token) {
    await res.status(400).json({ message: 'Not authenticate' });
    return;
  }
  try {
    const tokenPayload = jwt.verify(token, process.env.SECRET_KEY);
    req.user = {
      user: tokenPayload.user,
    };
    await next();
  } catch (err) {
    await res.status(400).json({ message: 'Token error' });
  }
};
module.exports = {
  authMiddleware,
};
