/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./client/src/data.js":
/*!****************************!*\
  !*** ./client/src/data.js ***!
  \****************************/
/***/ ((module) => {

eval("const BASE_URL = 'http://localhost:8080';\n\nconst REGISTER_URL = `${BASE_URL}/api/auth/register`;\n\nconst LOGIN_URL = `${BASE_URL}/api/auth/login`;\n\nmodule.exports = {\n  REGISTER_URL,\n  LOGIN_URL,\n};\n\n\n//# sourceURL=webpack://hw2/./client/src/data.js?");

/***/ }),

/***/ "./client/src/index.js":
/*!*****************************!*\
  !*** ./client/src/index.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

eval("const { REGISTER_URL, LOGIN_URL } = __webpack_require__(/*! ./data */ \"./client/src/data.js\");\n\nconst registerForm = document.getElementById('registerForm');\nif (registerForm) {\n  registerForm.addEventListener('submit', async (e) => {\n    e.preventDefault();\n    try {\n      const res = await fetch(REGISTER_URL, {\n        method: 'POST',\n        headers: {\n          'Content-Type': 'application/json',\n        },\n        body: JSON.stringify({\n          username: registerForm.username.value, password: registerForm.password.value,\n        }),\n      });\n      alert((await res.json()).message);\n      window.location.replace('http://localhost');\n    } catch (err) {\n      alert(`Error: ${err.message}`);\n    }\n  });\n}\n\nconst loginForm = document.getElementById('loginForm');\n\nloginForm.addEventListener('submit', async (e) => {\n  e.preventDefault();\n  const res = await fetch(LOGIN_URL,{\n    method: 'POST',\n    headers: {\n      'Content-Type': 'application/json',\n    },\n    body: JSON.stringify({\n      username: loginForm.username.value, password: loginForm.password.value,\n    }),\n  });\n  alert('s');\n  console.log(await res.json());\n});\n\n\n//# sourceURL=webpack://hw2/./client/src/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./client/src/index.js");
/******/ 	
/******/ })()
;