const {
  REGISTER_URL,
  LOGIN_URL,
  USER_URL,
  NOTES_URL,
} = require('./data');

const getToken = () => {
  const info = document.cookie;
  if (info.includes('jwt_token')) {
    const start = info.search('jwt_token=') + 'jwt_token='.length;
    const finish = info.slice(start).search(';');
    return `JWT ${info.slice(start, (finish === -1) ? undefined : finish + 1)}`;
  }
  return null;
};

const requestToNoteById = async (id, method) => {
  const res = await fetch(NOTES_URL + id, {
    method,
    headers: {
      'Content-Type': 'application/json',
      Authorization: getToken(),
    },
    body: (method.toUpperCase() === 'PUT')
      ? JSON.stringify({ text: document.getElementById('updateText').value }) : undefined,
  });
  if (method.toUpperCase() === 'GET') {
    const getNoteById = document.getElementById('getNoteById');
    getNoteById.innerText = JSON.stringify(await res.json());
  } else {
    alert((await res.json()).message);
  }
};

const registerForm = document.getElementById('registerForm');
if (registerForm) {
  registerForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    try {
      const res = await fetch(REGISTER_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: registerForm.username.value, password: registerForm.password.value,
        }),
      });
      alert((await res.json()).message);
      window.location.replace('http://localhost');
    } catch (err) {
      alert(`Error: ${err.message}`);
    }
  });
}

const loginForm = document.getElementById('loginForm');
if (loginForm) {
  loginForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    try {
      const res = await fetch(LOGIN_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: loginForm.username.value, password: loginForm.password.value,
        }),
      });
      if (res.status === 200) {
        const token = (await res.json()).jwt_token;
        document.cookie = `jwt_token=${token}`;
        alert('Success');
        window.location.replace('http://localhost');
      }
    } catch (err) {
      alert(err.message);
    }
  });
}

const getUsersInfo = document.getElementById('getUsersInfo');
if (getUsersInfo) {
  getUsersInfo.addEventListener('click', async (e) => {
    e.preventDefault();
    const res = await fetch(USER_URL, {
      method: 'GET',
      headers: {
        Authorization: getToken(),
        'Content-Type': 'application/json',
      },
    });
    const info = await res.json();
    if (res.status === 200) {
      const divInfo = document.getElementById('usersInfo');
      const element = document.createElement('p');
      // eslint-disable-next-line no-underscore-dangle
      element.innerText = `_id: ${info.user._id}
      username: ${info.user.username},`;
      divInfo.appendChild(element);
    }
  });
}

const changePasswordForm = document.getElementById('changePasswordForm');
if (changePasswordForm) {
  changePasswordForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    const res = await fetch(USER_URL, {
      method: 'PATCH',
      headers: {
        Authorization: getToken(),
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        oldPassword: changePasswordForm.oldPassword.value,
        newPassword: changePasswordForm.newPassword.value,
      }),
    });
    const { message } = await res.json();
    alert(message);
  });
}

const deleteUser = document.getElementById('deleteUser');
if (deleteUser) {
  deleteUser.addEventListener('click', async (e) => {
    e.preventDefault();
    const res = await fetch(USER_URL, {
      method: 'DELETE',
      headers: {
        Authorization: getToken(),
        'Content-Type': 'application/json',
      },
    });
    const { message } = await res.json();
    alert(message);
  });
}

const getUsersNotes = document.getElementById('getUsersNotes');
if (getUsersNotes) {
  getUsersNotes.addEventListener('click', async (e) => {
    e.preventDefault();
    const res = await fetch(NOTES_URL, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: getToken(),
      },
    });
    const notes = document.getElementById('notes');
    const info = await res.json();
    notes.innerText = JSON.stringify(info);
  });
}

const addNoteForm = document.getElementById('addNoteForm');
if (addNoteForm) {
  addNoteForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    const res = await fetch(NOTES_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: getToken(),
      },
      body: JSON.stringify({
        text: addNoteForm.text.value,
      }),
    });
    alert((await res.json()).message);
  });
}

const getNoteByIdForm = document.getElementById('getNoteByIdForm');
if (getNoteByIdForm) {
  getNoteByIdForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    await requestToNoteById(getNoteByIdForm.getId.value, 'GET');
  });
}

const updateNoteById = document.getElementById('updateNoteById');
if (updateNoteById) {
  updateNoteById.addEventListener('submit', async (e) => {
    e.preventDefault();
    await requestToNoteById(updateNoteById.updateId.value, 'PUT');
  });
}

const checkNoteById = document.getElementById('checkNoteById');
if (checkNoteById) {
  checkNoteById.addEventListener('submit', async (e) => {
    e.preventDefault();
    await requestToNoteById(checkNoteById.checkId.value, 'PATCH');
  });
}

const deleteNoteById = document.getElementById('deleteNoteById');
if (deleteNoteById) {
  deleteNoteById.addEventListener('submit', async (e) => {
    e.preventDefault();
    await requestToNoteById(deleteNoteById.deleteId.value, 'DELETE');
  });
}
