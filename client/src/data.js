const BASE_URL = 'http://localhost:8080';

const REGISTER_URL = `${BASE_URL}/api/auth/register`;

const LOGIN_URL = `${BASE_URL}/api/auth/login`;

const USER_URL = `${BASE_URL}/api/users/me`;

const NOTES_URL = `${BASE_URL}/api/notes/`;

module.exports = {
  REGISTER_URL,
  LOGIN_URL,
  USER_URL,
  NOTES_URL,
};
